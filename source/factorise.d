module factorise;

import std.algorithm;
import std.array;
import std.math;
import std.conv;

version (unittest) {
    import fluent.asserts;
}

struct Factor {
    int prime;
    int power;
}

Factor[] getFactors (int number) {
    Factor[] listOfFactors;

    for(int i = 2; i <= number; i++)
    {
        if(number % i == 0)
        {
            auto factor = Factor(i, 1);
            number = number/i;
            while(number % i == 0)
            {
                factor.power ++;
                number = number/i;
            }
            listOfFactors ~= factor;
        }
    }
    return listOfFactors;
}

int[] getSuccessivePrimesUpTo(int biggestPrime) {
    bool[int] isPrime;
    for (int i = 2; i <= biggestPrime; i++)
        isPrime[i] = true;

    for(int i = 2; i <= sqrt(biggestPrime.to!double).to!int; i++)
    {
        if(isPrime[i] == true)
        {
            for(int j = i * i; j <= biggestPrime; j += i)
            {
                isPrime[j] = false;
            }
        }
    }
    int[] primes;
    for (int i = 2; i <= biggestPrime; i++)
    {
        if(isPrime[i])
        {
            primes ~= i;
        }
    }
    return primes;
}

bool areSuccessivePrimes(int[] primes)
{
    int biggestPrime = primes[primes.length-1];
    int[] allPrimes = getSuccessivePrimesUpTo(biggestPrime);
    if(primes.length < allPrimes.length)
    {
        return false;
    }
    return true;
}

bool isGodelNumber(int number) {
    if(number <= 12)
        return true;

    auto factors = getFactors(number);
    if (factors.length == 1 && factors[0].power == 1)
    {
        return true;
    }

    auto primes = factors.map!(a => a.prime).array;
    return areSuccessivePrimes(primes);
}

///succcessive primes to 5 are [2, 3, 5]
unittest {
    getSuccessivePrimesUpTo(5).should.equal([2,3,5]);
}

///succcessive primes to 2 is [2]
unittest {
    getSuccessivePrimesUpTo(2).should.equal([2]);
}

///[2,5] are not successive primes
unittest {
    areSuccessivePrimes([2,5]).should.equal(false);
}

///[2,3] are successive primes
unittest {
    areSuccessivePrimes([2,3]).should.equal(true);
}

/// a composed number larger than 12 which does have as factors successive primes is Godel
unittest {
    isGodelNumber(243000000).should.equal(true);
}

/// a composed number larger than 12 which does not have as factors successive primes is not Godel
unittest {
    isGodelNumber(100).should.equal(false);
}

/// a prime number larger than 12 is a Godel number
unittest {
    isGodelNumber(13).should.equal(true);
    isGodelNumber(17).should.equal(true);
}

/// a prime number smaller than or equal to 12 is a Godel number
unittest {
    isGodelNumber(1).should.equal(true);
    isGodelNumber(12).should.equal(true);
}

/// 243000000 is decomposed as 2^6 * 3^5 * 5^6
unittest {
    getFactors(243000000).should.equal([Factor(2,6), Factor(3,5), Factor(5,6)]);
}

/// 9 is decomposed as 3^2
unittest {
    getFactors(9).should.equal([Factor(3,2)]);
}

/// 6 is decomposed as 2 * 3
unittest {
    getFactors(6).should.equal([Factor(2,1), Factor(3,1)]);
}

/// 3 is decomposed as 3
unittest {
    getFactors(3).should.equal([Factor(3,1)]);
}

/// 2 is decomposed as 2
unittest {
    getFactors(2).should.equal([Factor(2,1)]);
}
